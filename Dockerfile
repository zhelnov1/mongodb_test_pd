FROM node:19-alpine AS build

WORKDIR /src
COPY package*.json ./
RUN npm i --silent
COPY . .

#--------------
FROM node:19-alpine
WORKDIR /src
COPY package*.json ./
RUN npm i --silent
COPY --from=build /src .
EXPOSE 4000


CMD ["nmp", "start", "--reload"]
